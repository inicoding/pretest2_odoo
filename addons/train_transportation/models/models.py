# -*- coding: utf-8 -*-

from datetime import timedelta
from email.policy import default

from requests import session

from odoo import models, fields, api

class TrainCity(models.Model):
    _name = 'train.city'
    _description = 'Train City'

    name = fields.Char(string='Name')
    code_city = fields.Char(string='Code City')

class TrainStation(models.Model):
    _name = 'train.station'
    _description = 'Train Station'

    name = fields.Char(string='Name')
    code_station = fields.Char(string='Code Station')
    city_id = fields.Many2one('train.city', string='City')
    address = fields.Text(string='Address')

class TrainTrain(models.Model):
    _name = 'train.train'
    _description = 'Train Train'

    name = fields.Char(string='Name')
    code_train = fields.Char(string='Code')
    capacity = fields.Integer(string='Capacity')
 
    active = fields.Boolean(default=True)    
    
    schedule_line = fields.One2many('train.schedule', 'train_id', string='Information')
    state = fields.Selection([('ops1', 'Not ready'), ('ops2', 'Ready'), ('ops3', 'Maintenance')], string='State', readonly=True, default='ops1')    

    def action_confirm(self):
        self.write({'state': 'ops2'})

    def action_cancel(self):
        self.write({'state': 'ops1'})

    def action_close(self):
        self.write({'state': 'ops3'})

    
class TrainSchedule(models.Model):
    _name = 'train.schedule'
    _description = 'Train Schedule'

    @api.onchange('schedule_time')
    def onchange_schedule_time(self):
        if self.schedule_time:
            schedule_time = fields.Datetime.from_string(self.schedule_time)
            interval = timedelta(seconds=(self.duration * 3600))
            self.arrival_time = fields.Datetime.to_string(schedule_time + interval)

    # @api.depends('capacity', 'train_id')
    # def compute_taken_capacity(self):
    #     for sesi in self:
    #         sesi.taken_capacity = 0
    #         if sesi.capacity and sesi.train_id :
    #             sesi.taken_capacity = 100 * len(sesi.train_id) / sesi.capacity

    origin_id = fields.Many2one('train.station', string='Origin', required=True)
    destination_id = fields.Many2one('train.station', string='Destination', required=True)
    schedule_time = fields.Datetime(string='Schedule')
    duration = fields.Float(string='Duration')
    arrival_time = fields.Datetime(string='Arrival')
    train_id = fields.Many2one('train.train')
    capacity_id = fields.Many2one('train.train', 'Capacity', required=True, ondelete='cascade')
    # taken_capacity = fields.Float(string='Capacity', compute='compute_taken_capacity')

    ref = fields.Char(string='Nomor Code', readonly=True, default='/')

    @api.model
    def create(self, vals):
        vals['ref'] = self.env['ir.sequence'].next_by_code('train.schedule')
        return super(TrainSchedule, self).create(vals)
