from pkg_resources import require
from odoo import api, fields, models

class Partner(models.Model):
    _inherit = 'res.partner'

    instructor = fields.Selection([('passenger', 'Passenger'), ('machinist', 'Machinist')], string='State')
    trainstate_line = fields.One2many('train.state', 'identity', string='Personal Data')

    # train_state = fields.Selection([('passenger', 'Passenger'), ('machinist', 'Machinist')], string='State')
    # identity = fields.Char(string='Identity')
    # gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='State')
    # born_date = fields.Date(string='Born Date')

    
    # session_line = fields.One2many('training.session', 'partner_id', string='Daftar Mengajar Sesi', readonly=True)

class List(models.Model):
    _name = 'train.state'
    
    identity = fields.Char(string='Identity', required=True)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Gender', required=True)
    born_date = fields.Date(string='Born Date', required=True)
